# Eddie2

Eddie2 is a tool for analyzing differences between two versions of an XML DTD and writing XSLT to migrate documents.

## INSTALLAION

You need to install the following Perl packages:
    XML::Checker::Parser
    XML::Catalog

You can do this interactively in a terminal using:
    perl -MCPAN -e shell

## CONFIGURATION

Edit the configuration file (eddie2conf.txt by default); it's a text file
in a format similar to JSON (based on Perl maps, though).

Do *NOT* accept a conf file from someone you don't know; it can run arbitrary Perl commands on your system. This will change before releasing Eddie2

The conf file has comments explaining the format; there's a special section enar the start that's for options for Eddie2 itself.

## RUNNING

Run ./eddie2.pl --help to see the options; most common usage is to give to and from public identifiers and a catalog path.

Run ./eddie2.pl [options...] > lib/eddie2.xsl

Edide2 will create two stub documents and validate them against the DTD; this will fail and make lots of error messages, but it will also cause the library to read the respective DTDs.

You will end up with a report, by default in out/report.html, which will be an extensive HTML document explaining differences between the two DTDs. There will also be a (possibly quite large) xslt file as the output of the script.

You should make a new XSLT 3 file, and _import_ lib/eddie2.xsl in it. Then overwride any templates you need to, and mark those as MANUAL in the Eddie2 configuration file.

## The report

At the top there should be the source and destination public and/or system identifiers of the two DTDs, with differently coloured backgrouds.  This uses _report.css_; if thats missing, go find it.

There's some JavaScript generated in the report: pressing a letter (a-z) should take you to the first element whose name starts with that letter.


