#! /usr/bin/env perl
use strict;
use warnings;
use English;
use utf8;
use open ':std', ':encoding(UTF-8)'; # for stdout, stderr, stdin
use File::Basename; # for fileparse() or the less reliable dirname()

# Generate XSLT to convert from one DTD to another.
# eddie2
# Liam Quin, Delightful Computing
# $Id: eddie2.pl,v 2.20 2020/01/30 05:53:53 lee Exp lee $
#
# Usage:
# eddie2.pl [options]
#
# This program does NOT do a complete job. It handles cases where:
# * The elements are the same and so are all their attributes
# * Content models are the same but there are new attributes in the destination
#   (in that case you just won't get the new attributes)
#
# It DETECTS the following cases:
# * content models differ
# * one or more dest attributes is required but not required in the source dtd
#   (this includes an attribute becoming #FIXED or an enumeration changing)
# * element appears in source but not destination
#
# It also tries to detect elements in the destination but not the source
# -- will not appear but might be required.
# This is not yet handled well, and will need hand work.
#
# This script does not directly support XML catalogs.
# There may be bugs to do with parameter entities in conditionla sections,
# according to the man page.

use XML::Checker::Parser;
use XML::Catalog;
use Getopt::Long::Descriptive;

my ($opt, $usage) = describe_options(
    '%c %o [file1 file2]',
    [ "from-public:s", "input PUBLIC formal identifier for DTD" ],
    [ "from-system:s", "input PUBLIC system identifier for DTD" ],
    [ "to-public:s", "destination PUBLIC formal identifier for DTD" ],
    [ "to-system:s", "destination input PUBLIC system identifier for DTD" ],
    [ "from-element:s", "source document top-level element" ],
    [ "to-element:s", "destination document top-level element" ],
    [ "catalog-path:s", "semicolon-separated list of XML catalog files" ],
    [ "conf-file:s", "Eddie2 configuration file (must start with / ./ or ../)" ],
    [ "report-file:s", "Where to write the report (out/report.html)" ],
    [ "help",       "print usage message and exit", { shortcircuit => 1 } ]
);

print($usage->text), exit if $opt->help;

my $showitdidsomething = 0;

sub assembleXMLharness($$$);
sub showContentModelAndAttributes($$$);
sub estring($);
sub cm($);

my $conffile = $opt->{"conf_file"} || "./eddie2conf.txt";

my $catalog;
if ($opt->{catalog_path}) {
    $catalog = XML::Catalog->new(split(/;/, $opt->{catalog_path}));
}

my $reportfile = $opt->{"report_file"} || "out/report.html";

sub makefile($$)
{
    my ($opts, $which) = @_;

    if (defined $ARGV[$which]) {
	if (! -f $ARGV[$which] && $ARGV[$which] =~ m/^\s*<!DOCTYPE/) {
	    return $ARGV[$which];
	}
	local $/ = undef; # slurp mode
	open(my $fh, "<", $ARGV[$which]) or die "cannot open $ARGV[$which] for reading";
	my $text = <$fh>;
	close $fh;
	if ($text =~ m/<!DOCTYPE/) {
	    return $text;
	}
	die "XML instance file $ARGV[$which] did not include a <!DOCTYPE";
    }

    return assembleXMLharness(
	($which == 0) ? $opt->{"from_public"} : $opt->{"to_public"},
	($which == 0) ? $opt->{"from_system"} : $opt->{"to_system"},
($which == 0) ? ($opt->{"from_element"} || "from") : ($opt->{"to_element"} || "to")
    );
}

my %result;
my $resultkey;

my ($sourcefile, $destfile) = (makefile($opt, 0), makefile($opt, 1));
$result{source}{PUBLIC} = ($sourcefile =~ s/^.*PUBLIC \"([^\"]+)\".*$/$1/mr);
$result{dest}{PUBLIC} = ($destfile =~ s/^.*PUBLIC \"([^\"]+)\".*$/$1/mr);

# print "### source\n$sourcefile\n\n###dest\n$destfile\n";

sub xsltsame($;$$);
sub xsltdelete($$);
sub xsltdifferent($$;$);
sub inAbutnotB($$);
sub writeReportFile($$$);

# some special cases that are already handled
use constant COPY => 17;
use constant DELETE => 42;
use constant MANUAL => 97; # it's handled in XSLT written by hand
use constant EXPUNGE => 117; # remove content and tags,
                             # and don't report in content models

use constant WARN => "#";

my $AT = '@';

my %copyDescriptions;
BEGIN {
    # the + sign is to use the constants instead of barewords:
    $copyDescriptions{+COPY} = "copy unchanged";
    $copyDescriptions{+DELETE} = "remove element including children";
    $copyDescriptions{+MANUAL} = "see primary XSLT file";
    $copyDescriptions{+EXPUNGE} = "expunged [see Eddie2]";
    $copyDescriptions{+WARN} = "warn if found";
    print STDERR "done copyDescriptions\n";
}

my %copyUnchanged;

unless (%copyUnchanged = do $conffile) {
    die "couldn't parse conf file $conffile: $@" if $@;
    die "couldn't do $conffile: $!" unless %copyUnchanged;
}

print STDERR "** Eddie2: loaded conf file $conffile\n";

# ignore the following attributes everywhere:
my @ignoreAttributesEverywhere = (
    # "xlink:actuate",
    # "xlink:show",
    # "xlink:type"
);

my %expungedElements;

foreach (keys %copyUnchanged) {
    if ($_ =~ m/^\s*$/) {
	delete $copyUnchanged{$_};
    } elsif (exists $copyUnchanged{$_}  && ($copyUnchanged{$_} == EXPUNGE)) {
	$expungedElements{$_} = 1;
    }
}
my $expungePattern;

if (keys %expungedElements) {
    $expungePattern = join("|", keys %expungedElements);
}


my $ALLATS = "###"; # illegal attribute name we can use for a hash key
my %ignoreAttributesEverywhere = map { $_ => 1 } @ignoreAttributesEverywhere;

my $xmlns = "";
my $excludePrefixes = "";

sub config($)
{
    my ($key) = @_;

    if (exists $copyUnchanged{"##eddie##"}
	&& exists $copyUnchanged{"##eddie##"}{$key}) {
	return $copyUnchanged{"##eddie##"}{$key};
    } else {
	return undef;
    }
}

if (
    exists $copyUnchanged{"##eddie##"}
    && exists $copyUnchanged{"##eddie##"}{namespaces}) {

    my @ns = @{$copyUnchanged{"##eddie##"}{namespaces}};

    for (my $i = 0; $i < @ns; $i += 2) {
	$xmlns .= "xmlns:" . $ns[$i] . "=\"" . $ns[$i+1] . "\"\n";
	$excludePrefixes .= " " . $ns[$i];
    }
}


#### report
sub writepage(
)
{
    my (
	$elementName,
	$srcAtts,
	$dstAtts,
	$srcChildren,
	$dstChildren,
	$srcAppearsIn,
	$dstAppearsIn
    ) = @_;


}

sub readxmlfile($$)
{
    my ($key, $text) = @_;

    $resultkey = $key;

    open(my $fh, ">", "/tmp/boy.xml") or die "$!";
    print $fh $text;
    close $fh;

    my $parser = new XML::Checker::Parser(
	Handlers => {
	    Unparsed => \&xhp_Unparsed,
	    # Entity   => \&xhp_Entity,
	    Element => \&xhp_Element,
	    Attlist => \&xhp_Attlist
	},
	ParseParamEnt => 1,
	NoLWP => 1
    );

    eval {
	# local $XML::Checker::FAIL = \&my_fail;
	$| = 1;
	print STDERR "$0: ### parsing $key... validity errors are expected\n";
        $parser->parsefile("/tmp/boy.xml");
	print STDERR "$0: ### end of parse $key\n\n";
    };
    if ($@) {
	die "$0: exception in parsing $key: $@";
    }

    if ($showitdidsomething) {
	foreach (keys %{$result{$key}{element}}) {
	    my $h = \%{$result{$key}{element}};
	    foreach my $k (keys %$h) {
		print "$k MOD $h->{$k}{model}\n";
	    }
	}
    }
}

sub my_fail {
    my $code = shift;
    print STDERR "my_fail: $resultkey: ";
    XML::Checker::print_error($code, @_);
}

sub xhp_Unparsed($$$$$$)
{
    my ($Parser, $Entity, $Base, $Sysid, $Pubid, $Notation) = @_;

    print "$resultkey: UPE $Sysid, $Pubid\n";
}

sub xhp_Entity($$$$$$$)
{
    my ($Parser, $Name, $Val, $Sysid, $Pubid, $Ndata, $IsParam) = @_;

    if (defined $Sysid) {
	print "$resultkey: ENT $Name $Sysid\n";
    }

}

sub xhp_Element($$$)
{
    my ($Parser, $Name, $Model) = @_;

    my $prev = ""; my $kids = "";
    # $Model =~ s/[|]ruby[|]/|/g;
    # $Model =~ s/[|]fixed-case[|]/|/g;
    foreach (sort split /[\s,|+?*()]+/, $Model) {
	if ($_ ne $prev) {
	    if ($kids ne "") {
		$kids .= ", ";
	    }
	    $kids .= $_;
	    $prev = $_;
	}
    }

    if (exists $result{$resultkey}{element}{$Name}{children}) {
	die "$0: $resultkey duplicate definition for $Name";
    }
    # expunge EXPUNGE items
    if ($expungePattern && (", " . $kids . ", " =~ m/, ($expungePattern), /)) {
	$result{$resultkey}{element}{$Name}{originalchildren} = $kids;
	$kids = ", " . $kids . ", ";
	$kids =~ s/, /, , /g;
	$kids =~ s/, ($expungePattern), /, /g;
	$kids =~ s/, (, )+/, /g;
	$kids =~ s/^, (, )*//;
	$kids =~ s/, $//;
    }

    $result{$resultkey}{element}{$Name}{children} = $kids;

    # change (p)* to p*
    $Model =~ s#[(]([a-z0-9._-]+)[)]#$1#gi;

    if (config("sort-content-model-or-groups")) {
	$Model =~ s{
	    [(]\s*
	    (
		(?:
		    # at least one term: element name or #PCDATA
		    [#:a-z0-9_.-]+
		    (:?\s*[*?+])? # optional occurrence indicator
		)
		# optional additional terms; if there is only one,
		# sorting is useless but the substitution will normalize
		# whitespace:
		(?:
		    \s*[|]\s*
		    [#:a-z0-9_.-]+
		    (:?\s*[*?+])? # optional occurrence indicator
		)*
	    )
	    \s*[)]
	}{
	    "(" .  join('|', sort split /\s*[|]\s*/, $1) . ")"
	}egxi;
    }
    $result{$resultkey}{element}{$Name}{model} = "" . $Model;
}

sub xhp_Attlist($$$$$$)
{
    my ($Expat, $Elname, $Attname, $Type, $Default, $Fixed) = @_;

    if (exists $result{$resultkey}{element}{$Elname}{attributes}{$Attname}) {
	die "$0: $resultkey duplicate definition for $Elname att $Attname";
    }

    # if ($Attname =~ m/^xlink:/) { return }
    # if ($Attname =~ m/^xmlns:/) { return }
    # if ($Attname =~ m/^xml:/) { return }
    # if ($Attname =~ m/^mml/) { return }
    # if ($Attname eq "href") { return }
    if ($ignoreAttributesEverywhere{$Attname}) {
	return;
    }

    $result{$resultkey}{element}{$Elname}{attributes}{$Attname}{type} = $Type;
    $result{$resultkey}{element}{$Elname}{attributes}{$Attname}{default} = $Default;
    $result{$resultkey}{element}{$Elname}{attributes}{$Attname}{fixed} = $Fixed || 0;
}

sub xhp_Notation($$$$$)
{
    my ($Parser, $Notation, $Base, $Sysid, $Pubid) = @_;

    # print "NOT $Notation defined in $Base\n";
}

print STDERR " source file $sourcefile\n";
readxmlfile('source', $sourcefile);

print STDERR " dest file $sourcefile\n";
readxmlfile('dest', $destfile);

# now identify places where models *and* attributes are the same

my %onlyInFrom;

sub attdesc($$)
{
    my ($e, $attname) = @_;

    return $attname .
    " " . $e->{attributes}{$attname}{type} .
    " " . $e->{attributes}{$attname}{default} .
    " " . $e->{attributes}{$attname}{fixed};
}

sub attributedifferences($$)
{
    my ($attname, $e1, $e2) = @_;

    # We are only called on two attributes if they might
    # be different. If they are the same we return
    # the empty string.
    my $result = "";

    # cases:
    # (0) they are the same, yay
    #
    # (1) destination is required and source isn't;
    #     what do to if the source is missing it?
    # (2) destination is enumeration and source isn't:
    #     what to do about illegal values?
    # (3) both are enumeration and source has values not
    #     present in destination:
    #     what to do about illegal values?
    # (4) both are enumeration, source has extra values;
    #     does this represent required refinement?
    # (5) source is #FIXED and not a legal destination value
    # (6) source has default that is not a legal destination value

    if (($e2->{default} eq '#REQUIRED')  && ($e1->{default} ne '#REQUIRED')) {

	# if src has fixed, then 
    }

    if ($e1->{type} ne $e2->{type}) {
	if ($e2->{type} =~ m/^[(].*[|].*[)]$/) {
	    # the new attribute is an enumeration
	}
    }
}

sub breakablestring($)
{
    my ($input) = @_;

    return ($input =~ s/[|,+?*]/$&\&#8203;/gr);
}

{
    my $dir = (fileparse($reportfile))[2];
    if ($dir && $dir ne "" && $dir ne "." && ! -d "$dir/.") {
	# $dir/. so it works with a symlink to a directory
	mkdir "$dir"
	    or die "could not make directory $dir for report file $reportfile: $!";
    }
}

open(my $report, ">", $reportfile)
    or die "writeReportFile: can't create or write to out/report/report.html: $!";

binmode($report, ":utf8");

{ # make header, nav and start main for report
    # ARGV fix source/dest
print $report <<EOF;
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>E2 report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="report.css" />
  </head>
  <body>
EOF

    print $report "<nav><ul>\n";
    foreach my $e (sort (keys %{$result{source}{element}})) {
	my $from = $result{source}{element}{$e};
	my $to = $result{dest}{element}{$e};

	$from->{$ALLATS} = "";

	if (exists $from->{attributes}) {
	    my $all = join(" ", map {
		    attdesc($from, $_)
	    } sort keys %{$from->{attributes}});
	    $all =~ s/^\s+//;
	    $from->{$ALLATS} = $all;
	}

	if ($to) {
	    $to->{$ALLATS} = "";
	    if ( exists $to->{attributes}) {
		my $all = join(" ", map {
			attdesc($to, $_)
		} sort keys %{$to->{attributes}});
		$all =~ s/^\s+//;
		$to->{$ALLATS} = $all;
	    }

	}

	$from->{SAME} = ($to && (
		($from->{model} eq $to->{model}) &&
		($from->{$ALLATS} eq $to->{$ALLATS})
	    )
	) ? 1 : 0;

	my $sameclass = ($from->{SAME})? "same" : "different";
	my $handled = (exists $copyUnchanged{$e});
	my $icon = ($handled) ? "✔" : "✖";
	my $iconclass = ($handled) ? "handled" : "unhandled";
	my $icontitle = ($handled) ? "configured" : "not included explicitly in configuration";
	if ($handled) {
	    if (defined $copyDescriptions{$copyUnchanged{$e}}) {
		# $icontitle .= ": " . $copyDescriptions{$copyUnchanged{$e}};
		# this will copy an ARRAY, not what we want
	    } else {
		$icontitle .= ": " . $copyUnchanged{$e} . ": see main XSLT file";
	    }
	}

	print $report "  <li class=\"${sameclass} $iconclass\"><span title=\"$icontitle\" class=\"$iconclass\">$icon</span><a class=\"e\" href=\"#e-$e\">$e</a></li>\n";
    }

    my $from = "SYSTEM ";
    if ($opt->{from_public}) {
	$from = "PUBLIC " . $opt->{from_public} . " ";
    }
    $from .= $opt->{from_system};

    my $to = "SYSTEM ";
    if ($opt->{to_public}) {
	$to = "PUBLIC " . $opt->{to_public} . " ";
    }
    $to .= $opt->{to_system};

    print $report <<EOF;
    </ul></nav>
    <main>
    <h1>Eddie2 Report</h1>
    <section class="intro">
      <p class="snotd">$from</p>
      <p class="dnots">$to</p>
    </section>
EOF
} # nav and main for report

print <<EOF;
<?xml version="1.1" encoding="UTF-8"?>
<xsl:stylesheet
  version="3.0"
  xmlns:ali="http://www.niso.org/schemas/ali/1.0/"
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:lq="http://www.delightfulcomputing.com/ns/"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  $xmlns
  exclude-result-prefixes="xs lq map ali xsi xs oasis $excludePrefixes"
  >

  <!--* This file was generated by Eddie 2 using eddie2.conf
      *
      * DO NOT EDIT THIS FILE
      *
      * Copy any template you need into first.xsl instead.
      *
      * Liam Quin, Delightful Computing, 2019
      *
      *-->

EOF

sub highlight($$$)
{
    my ($input, $snotd, $dnots) = @_;

    if (!defined $snotd) {
	die "snotd undefined in $input";
    }
    if (!defined $dnots) {
	die "snotd undefined in $input";
    }
    my %snotd = map { $_ => 1 } split /[,\s]+/, $snotd;
    my %dnots = map { $_ => 1 } split /[,\s]+/, $dnots;

    my $result = "";

    while ($input =~ m/([^a-zA-Z0-9._-]*)([a-zA-Z0-9._-]+)([^a-zA-Z0-9._-]*)/g) {
	$result .= $1;

	if (exists $snotd{$2}) {
	    $result .= "<span class='del' title='source, not destination'>$2</span>";
	} elsif (exists $dnots{$2}) {
	    $result .= "<span class='ins' title='destination but not source'>$2</span>";
	} else {
	    $result .= $2;
	}
	$result .= $3;
    }
    return $result;
}


{
    my $err = 0;
    foreach (keys %copyUnchanged) {
	next if ($_ eq "##eddie##");
	if (!defined $result{source}{element}{$_}) {
	    print STDERR "$0: configuration $conffile has $_ not in dtd\n";
	    $err++;
	}
    }
    if ($err) {
	print STDERR "$0: error count $err, not continuing\n";
	exit 1;
    }
}

foreach my $e (sort (keys %{$result{source}{element}})) {
    my $urchins = $result{source}{element}{$e}{children};

    foreach (split /[,\s]+/, $urchins) {
	push @{$result{source}{foundin}{$_}}, $e;
    }
}
foreach my $e (sort (keys %{$result{dest}{element}})) {
    my $urchins = $result{dest}{element}{$e}{children};

    foreach (split /[,\s]+/, $urchins) {
	push @{$result{dest}{foundin}{$_}}, $e;
    }
}

sub copyDescription($$)
{
    my ($report, $e) = @_;

    if (!exists $copyUnchanged{$e}) {
	print $report "<p>$e not in configuration file</p>\n";
	return;
    }

    my $action = $copyDescriptions{$copyUnchanged{$e}};

    if (defined $action) {
	print $report "<p>Configured: $copyDescriptions{$copyUnchanged{$e}}</p>";
    } elsif (ref($copyUnchanged{$e}) eq 'ARRAY') {
	print $report "<p>Configured: copy with specific warnings:</p>";
	my @arr = @{$copyUnchanged{$e}};
	for (my $i = 0; $i < $#arr; $i += 2) {
	    print $report "<p>when ($arr[$i]) say $arr[$i + 1]</p>";
	}
    } else {
	print $report "<p>[not in configuration; default is COPY]</p>";
    }
}

foreach my $e (sort (keys %{$result{source}{element}})) {
    #
    # TODO use list of all elements here and have a config option
    # to show only ones in the src, or both (or only ones in dest???
    #
    my $contentmodeldifferences = "";
    my $attributedifferences = "";
    my $whatchanged = "";

    my $from = $result{source}{element}{$e};
    my $to = $result{dest}{element}{$e};

    if (exists $copyUnchanged{$e} && $copyUnchanged{$e} == MANUAL) {
	print "<!--* element $e see first.xslt *-->\n\n";
    } else {
	print "<xsl:template match=\"$e\">\n";
    }

    {
	my $sameclass = ($from->{SAME})? "same" : "different";
	print $report "  <section id='e-$e' class='element-section $sameclass'>\n";
	print $report "    <h1>$e</h1>\n";
	copyDescription($report, $e);
    }

    print "  <!--* Notes from Eddie2\n";
    my @actions;

    if (!$to) {
	$onlyInFrom{$e} = 1;

	$whatchanged = "[element only in source DTD]";
	print $report "    <p class='whence'>This element is only in the source DTD</p>\n";
	my $urchins = estring($result{source}{element}{$e}{children});
	my $hl = estring(cm($from->{model}));
	    print $report <<EOF;
     <div class="children diff">
       <p class='whence'>Only in the source DTD; children:</p>
       <p class="snotd">$urchins</p>
     </div>
     <div class='model'>
       <p class='diff contentmodel snotd'><e>$e</e>$hl</p>
     </div>
EOF
	showContentModelAndAttributes($report, 'source', $e);
	push @actions, ( "self::$e", "$e: Element only in source" );

	# If it's only in the source we have a problem.
	# E.g. oasis tables!
	#
	# Note: if it's only in the destination we can ignore it here,
	# as we won't see it in the input (except in error).
	# But since we are iterating over elements in src, we know
	# that's not the case.
    } else {
	# The element is in both src and dest.
	# (we do not have to handle the case of elements only in
	# the destination DTD because they will not be in the input;
	# this is part of the purpose of using a DTD)
	# #
	# So, first we compare attributes;
	# then, we compare content models.

	# only for debugging:
	# print $report "    <p class='whence'>This element is in both DTDs</p>\n";

	my ($snotd, $dnots) = (
	    inAbutnotB($from->{children}, $to->{children}) ,
	    inAbutnotB($to->{children}, $from->{children})
	);
	if ($from->{children} ne $to->{children}) {
	    my $diff = "diff"; # css class
	    my $diffnote = "";

	    $whatchanged .= "[children]";
	    print "      * children of element $e differ:\n";
	    # print "\tfrom $from->{children}\n";
	    # print "\t  to $to->{children}\n";
	    print "      *   in src not dest: $snotd\n";
	    print "      *   in dest not src: $dnots\n";
	    my $anyUnhandled = 0;
	    foreach (split /[\s,]+/, $snotd) {
		push @actions, ( $_, "$e contains child $_ not in destination DTD" );
		if (!exists $copyUnchanged{$_}) {
		    $anyUnhandled = 1;
		}
	    }
	    if (($dnots eq "") && !$anyUnhandled) {
		$diff = "same";
		$diffnote = "<p class=\"snotd\">(all these children are in the Eddie2 configuration already)</p>";
	    }

	    my $linksinsnotd = estring($snotd);

	    print $report <<EOF;
     <div class="children $diff">
       <p class='whence'>Children differ</p>
       <p class="snotd">$linksinsnotd</p>
       <p class="dnots">$dnots</p>$diffnote
     </div>
EOF
	} # children differ


	if ($from->{model} ne $to->{model}) {
	    $whatchanged .= "[model]";
	    my $how = "Complex models differ";

	    if ($from->{model} =~ m/^[(][^()]+[)][*+?]?$/
	       && $to->{model} =~ m/^[(][^()]+[)][*+>]?$/) {
		$how = "Or-groups with different children";
	    }

	    print "$e: $how\n";

	    print "      *   src: $from->{model}\n";
	    print "      *   dst: $to->{model}\n";

	    my $h1 = estring(cm(highlight($from->{model}, $snotd, $dnots)));
	    my $h2 = estring(cm(highlight($to->{model}, $snotd, $dnots)));
	    print $report <<EOF;
     <div class='model'>
       <p class='how'>${how}</p>
       <p class='diff contentmodel snotd'><e>$e</e>$h1</p>
       <p class='diff contentmodel dnots'><e>$e</e>$h2</p>
     </div>
EOF
	} else {
	    # same
	    my $h1 = estring(cm($from->{model}));
	    print $report <<EOF;
     <p>Content model is the same in source and destination:</p>
     <div class='model'>
       <p class='diff contentmodel snotd'><e>$e</e>$h1</p>
     </div>
EOF
	}
	if ($from->{$ALLATS} ne $to->{$ALLATS}) {
	    $whatchanged .= "[attributes]";
	    my $fromatts = join(' ', sort keys %{$from->{attributes}});
	    my $toatts = join(' ', sort keys %{$to->{attributes}});

	    print "      * differ: atts $e\n";
	    print $report "<div class='attributes diff'>\n";
	    print $report "<p>Attributes differ:</p>\n";

	    my $prunt = 0;
	    my ($snotd, $dnots) = (
		inAbutnotB($fromatts, $toatts),
		inAbutnotB($toatts, $fromatts)
	    );

	    if ($snotd ne "" or $dnots ne "") {
		print $report "    <div class='attlist diff'>\n";
		if ($snotd ne "") {
		    print "      *   atts in src but not dst: $snotd\n";
		    print $report "      <p class='atts snotd'>$snotd</p>\n";
		    $prunt = 1;
		    foreach (split /[\s,]+/, $snotd) {
			if ($_ !~ m/[:]/) {
			    push @actions, ( "@" . $_, "element $e has attribute \@$_ not in destination DTD" );
			}
		    }
		}
		if ($dnots ne "") {
		    print "      *   atts in dst but not src: $dnots\n";
		    print $report "      <p class='atts dnots'>$dnots</p>\n";
		    $prunt = 1;
		}
		print $report "    </div>\n";
	    }

	    # If the elements take different attributes,
	    # we have to deal with that.
	    # We know there are differences...

	    my $summary = "";
	    my $reportsummary = "";

	    # we'll make a set of attributes in the source that
	    # we have not yet handled:
	    my %fromatts;
	    foreach my $k (keys %{$from->{attributes}}) {
		$fromatts{$k} = 1;
	    }

	    # Now, look for required attributes in the destination
	    # that are not present in the source
	    print $report "<div class=\"attributediffs\">\n";
	    foreach my $k (sort keys %{$to->{attributes}}) {
		if (!exists $fromatts{$k}) {
		    # if the destination has an attribute the source does not,
		    # either an attribute was renamed or one was added.
		    # We will treat a rename as a delete and an addition,
		    # implying a human check.
		    # A new attribute only affects us in conversion if it
		    # is required, as we won't have it.
		    if ($to->{attributes}{$k}{default} eq "#REQUIRED") {
			if ($summary eq "") {
			    $summary = "In destination but not source: ";
			    $reportsummary = "<p>In destination but not source:</p>\n";
			}
			$summary .= " [" . attdesc($to, $k) . "]";
			$reportsummary .= "<p class=\"att snotd\">" . attdesc($to, $k) . "</p>\n";
			push @actions, ( "not(\@" . $k . ")",
			    "element $e is missing attribute \@$k but this is required in the destination DTD" );
		    }
		} else { # in both, check types are compatible
		    if ($to->{attributes}{$k}{default} eq "#REQUIRED" &&
				    $from->{attributes}{$k}{default} ne "#REQUIRED") {

			print "      * $e att $k now required:\n";
			print "      *        " . $to->{attributes}{$k}{type} . "\n";
			print "      *        " . $to->{attributes}{$k}{default} . "\n";
			print $report <<EOF;
    <p class="incompatible">attribute $k became #REQUIRED</p>
    <p class="snotd">$to->{attributes}{$k}{type}</p>
    <p class="dnots">$to->{attributes}{$k}{default}</p>
EOF
		    } elsif ($to->{attributes}{$k}{fixed} eq "1" &&
				    $from->{attributes}{$k}{fixed} ne "1") {
			# this commonly happens with namespace bindings,
			# putting xmlns:foo="..." in the DTD
			print "      * $e att $k now has fixed value " .
				    $to->{attributes}{$k}{type} . "\n";
			print $report "<p>Attribute $k now has #FIXED value $to->{attributes}{$k}{type}</p>\n";
		    } elsif ($to->{attributes}{$k}{type} =~ m{^[(].*[)]$}) {

			# enumeration in destination
			if ($from->{attributes}{$k}{type} =~ m{^[(].*[)]$}) {
			    # they are both enumerations
			    my $fromgroup = $from->{attributes}{$k}{type};
			    my $togroup = $to->{attributes}{$k}{type};

			    if ($fromgroup ne $togroup) {
				$fromgroup =~ s/\s*[()|]+\s*/ /g;
				$togroup =~ s/\s*[()|]+\s*/ /g;
				my $lost = inAbutnotB($fromgroup, $togroup);
				my $gained = inAbutnotB($togroup, $fromgroup);
				if ($lost ne "") {
				    print "      * ### src \@$k has $lost\n";
				    print $report "<p class=\"snotd\">Attribute $k values no longer allowed: $lost</p>\n";
				    if ($gained eq "") {
					print "      * ### dst \@$k [none gained]\n";
				    }
				}
				if ($gained ne "") {
				    if ($lost eq "") {
					print "      * ### src \@$k [none lost]\n";
				    }
				    print "      * ### dest \@$k gained $gained\n";
				    print $report "<p class=\"dnots\">Attribute $k values only in destination: $gained</p>\n";
				}
			    }
			}
		    }
		}
	    }
	    print $report "</div>\n";
	    if ($summary ne "") {
		print "      * $summary\n";
		$summary = "";
		$prunt = 1;
	    }
	    if ($reportsummary ne "") {
		print $report $reportsummary;
		$reportsummary = "";
	    }
	    foreach (sort keys %fromatts) {
		if (!exists $to->{attributes}{$_}) {
		    if ($summary eq "") {
			$summary = "In source but not destination: ";
			print $report "<p>Attributes in source but not destination:</p>\n";
		    }
		    $summary .= " [" . attdesc($from, $_) . "]";
		    $reportsummary .= "<p class=\"att snotd\">" . attdesc($from, $_) . "</p>\n";
		    $prunt = 0;
		}
	    }
	    if ($summary ne "") {
		print "      * $summary\n";
		print "      * toatts: " . $to->{$ALLATS} . "\n";
		$summary = "";
		$prunt = 1;
	    }
	    if ($reportsummary ne "") {
		print $report "<div class=\"diff\">" . $reportsummary . "</div>\n";
		$reportsummary = "";
	    }

	    if (!$prunt) {
		print "       *  differ in types\n";
		print "       *  from: " .
		    $from->{$ALLATS} . "\n";
		print "       *    to: " .
		    $to->{$ALLATS} . "\n";
	    }
	    print $report "</div>\n";
	} elsif (!defined $from->{$ALLATS} || ($from->{$ALLATS} =~ m/^\s*$/)) {
	    print $report "<p>No attributes in source or destination.</p>\n";
	}  else {
	    print $report "<p>Attributes are the same in source and destination.</p>\n";
	}
	showContentModelAndAttributes($report, 'both', $e);
    }
    if ($from->{SAME}) {
	print "      * [same]\n";
	print $report "<p class=\"same\">This element has the same attributes and content in both DTDs</p>\n";
    }
    print "      *-->\n";
    if (exists $copyUnchanged{$e} && $copyUnchanged{$e} == MANUAL) {
	print $report <<EOF;
	<div class="config"><p>Element $e marked in configuration to be given special processing</p></div>
    </section>
EOF
	print "\n";
	next;
    }
    if (exists $copyUnchanged{$e}) {
	my $actions = $copyUnchanged{$e};
	if ($actions == COPY) {
	    xsltsame($e, "manual analysis suggested no change");
	} elsif ($actions == DELETE || $actions == EXPUNGE) {
	    xsltdelete($e, "manual override: delete tags and content");
	} elsif (ref($actions) eq "ARRAY") {
	    # an array of Xath expressions to warn about
	    xsltsame($e, "manual analysis: copy with checks", $actions);
	}
    } elsif ($from->{SAME}) {
	if (exists $copyUnchanged{$e}) {
	    print STDERR "copy $e\n";
	}
	xsltsame($e, "automatic analysis: no change", \@actions);
    } else {
	xsltdifferent($e, $whatchanged, \@actions);
    }
    print "</xsl:template>\n\n";
    print $report "  </section>\n";
}

sub printSummary(%)
{
    my (%info) = @_;

    if ($info{source} eq "src") {
    }
}

# while (<DATA>) {
# print;
# }
 
print "</xsl:stylesheet>\n";

# could print an attribute summary here in the report?
# e.g., for each attribute
# @colour used on hair, [shoes], socks
#

print $report <<'EOF';
<hr />
<p class="about">Eddie 2 module: <small> \$Id: eddie2.pl,v 2.20 2020/01/30 05:53:53 lee Exp lee $ </small></p>
EOF
while (<DATA>) {
    print $report $_;
}
print $report "</main></body></html>\n" or die "can't write to report: $!";
$report->close() or die "can't close out/report.html: $!";

print STDERR "Eddie2 ended OK\n";

exit 0;

#################

sub inAbutnotB($$)
{
    my ($a, $b) = @_;
    my %inB;

    my @a = split(/[,\s]+/, $a);
    my @b = split(/[,\s]+/, $b);

    map { $inB{$_} = 1; } @b;

    my @result;
    foreach (@a) {
	if (!exists $inB{$_}) {
	    push @result, $_;
	}
    }

    return join(", ", sort @result) || "";
}

sub quotequotes($)
{
    my ($string) = @_;

    $string =~ s/"/\&quot;/g;
    return $string;
}

sub xsltsame($;$$)
{
    my ($e, $reason, $warnings) = @_;

    if (defined $reason) {
        print "    <!--* $reason *-->\n";
    }
    if (defined $warnings && ref($warnings) eq 'ARRAY') {
	my @arr = @$warnings;
	for (my $i = 0; $i < $#arr; $i += 2) {
	    my $expr = quotequotes($arr[$i]);
	    my $warn = $arr[$i + 1];
	    if ($warn eq WARN) {
	      $warn = "contains disallowed element $expr";
	    }
	    print <<EOF
    <xsl:if test="$expr">
      <xsl:message expand-text="yes">$e: $warn</xsl:message>
    </xsl:if>
EOF
	}
    }
    if (config("copy-elements-by-rewriting")) {
	my $func = "name()";
	if (config("remove-default-namespace")) {
	    $func = "local-name()";
	}
	print <<EOF;
	<xsl:element name="{${func}}">
	  <xsl:apply-templates select="\@* | node()"/>
	</xsl:element>
EOF
    } else {
	print <<'EOF';
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
EOF
    }
}

sub xsltdelete($$)
{
    my ($e, $reason) = @_;

    if (defined $reason) {
        print "    <!--* element $e: $reason *-->\n";
    }

    print <<'EOF';
    <!--* no action: delete the tags and their content *-->
EOF
}

sub xsltdifferent($$;$)
{
    my ($e, $what, $warnings) = @_;

    my $end = "</xsl:element>";

    my $inDest = (defined $result{dest}{element}{$e});

    if (!$inDest) {
	$end = "<!--* not in destination so process children *-->";
    } elsif (config("copy-elements-by-rewriting")) {
	my $func = "name()";
	if (config("remove-default-namespace")) {
	    $func = "local-name()";
	}
	print <<EOF;
	<xsl:element name="{${func}}">
	  <xsl:apply-templates select="@* | node()"/>
EOF
    } else {
	$end = "</xsl:copy>";
	print <<EOF;
	<xsl:copy>
	  <xsl:apply-templates select="@*" />
EOF
    }
      if (defined $warnings && ref($warnings) eq 'ARRAY') {
	  my @arr = @$warnings;
	  for (my $i = 0; $i < $#arr; $i += 2) {
	      my $expr = quotequotes($arr[$i]);
	      my $warn = $arr[$i + 1];
	      if ($warn eq WARN) {
		$warn = "contains disallowed element $expr";
	      }
	      print <<EOF
      <xsl:if test="$expr">
	<xsl:message expand-text="yes">$e: $warn</xsl:message>
      </xsl:if>
EOF
	  }
      } else {
	  print <<EOF;
      <xsl:message>Not handled [different]: $e - $what</xsl:message>
EOF
      }
      print <<EOF;
      <xsl:apply-templates select="node()"/>
      $end
EOF
}

sub e($)
{
    my ($e) = @_;

    return "<a class=\"e\" href=\"#e-$e\">$e</a>";
}

sub elist(@)
{
    my (@list) = @_;
    return join(", ", map { e($_) } @list);
}
our %hide;
our $hide_n = 0;
sub Hide($) {
    my ($input) = @_;

    ++$hide_n;
    $hide{$hide_n} = $input;
    return "«" . $hide_n . "»";
}

sub Unhide($)
{
    my ($n) = @_;

    return $hide{$n};
}

sub estring($)
{
    my ($input) = @_;
    $hide_n = 0;

    my $before = $input;
    $before =~ s/#PCDATA/Hide($&)/ge;
    $before =~ s/<[^<>]+>/Hide($&)/ge;
    $before =~ s{[A-Z_][a-z_0-9.-]*}{<a class="e" href="#e-$&">$&</a>}gi;
    $before =~ s/«(\d+)»/Unhide($1)/ge;
    return $before;
}

sub cm($)
{
    # expand content model
    my ($line) = @_;

    $hide_n = 0;
    $line =~ s/<[^<>]+>/Hide($&)/ge;

    $line =~ tr/()/{}/;
    my $n = 0;
    while ($line =~ m/{.*}/) {
	$line =~ s{{([^{}]*)}}{<g class="d$n">(<c>$1</c>)</g>}g;
	++$n;
    }
    $line =~ s/«(\d+)»/Unhide($1)/ge;
    return breakablestring($line);
}


sub showContentModelAndAttributes($$$)
{
    my ($report, $whence, $e) = @_;

    if (exists $result{source}{element}{$e}{attributes}) {
	print $report "<div class=\"attdecls $whence\">\n";
	foreach (sort keys %{$result{source}{element}{$e}{attributes}}) {
	    my $class = (exists $result{dest}{element}{$e}{attributes}{$_}) ? "both" : "snotd";
	    my $fixed = ($result{source}{element}{$e}{attributes}{$_}{fixed}) ?  "<f>#FIXED</f>" : "";
	    print $report "<div class=\"attrdef $class\">";
	    print $report "<attr>$_</attr> ";
	    print $report "<type>$result{source}{element}{$e}{attributes}{$_}{type}</type>";
	    print $report "<default>$result{source}{element}{$e}{attributes}{$_}{default}</default>$fixed";
	    print $report "</div>\n";
	}
	foreach (sort keys %{$result{dest}{element}{$e}{attributes}}) {
	    # if (!exists $result{source}{element}{$e}{attributes}{$_}) {
		my $fixed = ($result{dest}{element}{$e}{attributes}{$_}{fixed}) ?  "<f>#FIXED</f>" : "";
		print $report "<div class=\"attrdef dnots\">";
		print $report "<attr>$_</attr> ";
		print $report "<type>$result{dest}{element}{$e}{attributes}{$_}{type}</type>";
		print $report "<default>$result{dest}{element}{$e}{attributes}{$_}{default}</default>$fixed";
		print $report "</div>\n";
		# }
	}
	print $report "</div>\n";
    }

    if (!exists $result{source}{foundin}{$e}) {
	print $report <<EOF;
	<div class="foundin">
	  <p>Element $e is not found in the content of any other element.</p>
	</div>
EOF
	return;
    }
    my $parents = elist(@{$result{source}{foundin}{$e}});
    print $report <<EOF;
    <div class="foundin">
      <p>Element $e is found in:</p>
      <p class="snotd foundinlist">$parents</p>
EOF
    my $newparents = elist(@{$result{dest}{foundin}{$e}});
    if ($parents ne $newparents) {
	print $report "      <p class=\"dnots\">$newparents</p>\n";
    } else {
	print $report "      <p>[same in both source and destination]</p>\n";
    }
    print $report "    </div>\n";
}

sub assembleXMLharness($$$)
{
    my ($public, $system, $e) = @_;

    if (!defined $system) {
	die "You must provide a system identiifier for both source and destination";
    }
    my $doc = "<!DOCTYPE $e ";
    if ($public) {
	if ($catalog) {
	    my $filename = $catalog->resolve_public($public);
	    if (!$filename) {
		die "catalog resolution failed for $public using catalog $opt->{catalog_path}";
	    }
	    $filename =~ s{^file://*}{/};
	    $doc .= "SYSTEM \"$filename\"";
	} else {
	    $doc .= "PUBLIC \"$public\" \"$system\"";
	}
    } else {
	# $doc .= "SYSTEM \"$system\"";
	$doc .= "[\n<!ENTITY \% dtd SYSTEM \"$system\">\n\%dtd;\n]";
    }
    return $doc . "><$e></$e>\n";
}

__DATA__
<script>
// JavaScript for report
// bind keys a to z to go to the first element starting with that leatter

document.onkeydown = function(event) {
        event = event || window.event;

    var key, target;
    console.log("Eddie2: keypress");

    if (window.event) {
	key = window.event.keyCode;
	target = window.event.srcElement;
    } else if (event.which) {
	key = event.which;
	target = event.target;
    } else {
	console.log("Eddie2: unexpected event ignored");
	return true; // unexpected
    }

    if (!key) {
	console.log("Eddie2: no key");
	return true; // e.g. the alt key by itself
    }

    if (event.ctrlKey || event.altKey || event.metaKey) {
	console.log("Eddie2: modified key ignored");
	return true; // not for us
    }

    if (key < 65 || key > (65 + 26)) {
	console.log("Eddie2: out of range: " + key);
	return true;
    }

    var ch = String.fromCharCode(key).toLowerCase();
	console.log("you pressed " + ch);

    var headings = document.querySelectorAll("html>body>nav>ul>li>a");

    for (var i = 0; i < headings.length; i++) {
	var href = "" + headings[i].getAttribute("href");
	var hh = href.substring(3, 4).toLowerCase();
	if (hh == ch) { // e.g. i in "#e-img"
	    var here = document.location + "#";
	    here = here.replace(/#.*$/, href);
	    document.location = here;
	    headings[i].scrollIntoView();
	    return false; // handled
	}
    }

    return true;
}
console.log("Eddie2 report: loaded js");
</script>
